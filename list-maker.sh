#!/bin/bash

# https://apps.azcorrections.gov/mugshots/300000.jpg

for ((i=1;i<=500000;i++)); do
    echo "https://apps.azcorrections.gov/mugshots/$i.jpg" >> url.lst
done