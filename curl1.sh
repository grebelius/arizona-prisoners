#!/bin/bash

# curl -o /dev/null --silent --head --write-out '%{http_code}\n' https://apps.azcorrections.gov/mugshots/30000.jpg

xargs -n1 -P 100 curl -o /dev/null --silent --head --retry 4 --write-out '"%{url_effective}","%{http_code}","%{time_total}","%{time_namelookup}","%{time_connect}","%{size_download}","%{speed_download}"\n' < url-test.lst | tee results.csv

awk -F, '$2=200' results.csv | awk '{print $1}' | grep -o -E '[0-9]+'